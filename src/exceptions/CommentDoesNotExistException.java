package exceptions;

@SuppressWarnings("serial")
public class CommentDoesNotExistException extends Exception {
	public CommentDoesNotExistException() {
		super();
	}

	public CommentDoesNotExistException(String msg) {
		super(msg);
	}
}

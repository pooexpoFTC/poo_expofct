package exceptions;

@SuppressWarnings("serial")
public class DepartmentDoesNotExistException extends Exception {
	public DepartmentDoesNotExistException() {
		super();
	}

	public DepartmentDoesNotExistException(String msg) {
		super(msg);
	}
}

package exceptions;
@SuppressWarnings("serial")
public class NotRegisteredInEventException extends Exception {
	public NotRegisteredInEventException() {
		super();
	}

	public NotRegisteredInEventException(String msg) {
		super(msg);
	}
}

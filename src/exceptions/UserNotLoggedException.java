package exceptions;

@SuppressWarnings("serial")
public class UserNotLoggedException extends Exception {
	public UserNotLoggedException() {
		super();
	}

	public UserNotLoggedException(String msg) {
		super(msg);
	}
}

package exceptions;

@SuppressWarnings("serial")
public class UserCannotExecuteException extends Exception {
	public UserCannotExecuteException() {
		super();
	}

	public UserCannotExecuteException(String msg) {
		super(msg);
	}
}


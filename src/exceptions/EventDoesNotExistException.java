package exceptions;
@SuppressWarnings("serial")
public class EventDoesNotExistException extends Exception {
	public EventDoesNotExistException() {
		super();
	}

	public EventDoesNotExistException(String msg) {
		super(msg);
	}
}
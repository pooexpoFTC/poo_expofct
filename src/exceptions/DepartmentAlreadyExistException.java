package exceptions;

@SuppressWarnings("serial")
public class DepartmentAlreadyExistException extends Exception {
	public DepartmentAlreadyExistException() {
		super();
	}

	public DepartmentAlreadyExistException(String msg) {
		super(msg);
	}
}

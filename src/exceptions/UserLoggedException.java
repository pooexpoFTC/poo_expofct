package exceptions;

@SuppressWarnings("serial")
public class UserLoggedException extends Exception{
	public UserLoggedException(){
		super();
	}
	public UserLoggedException(String msg){
		super(msg);
	}
}

package exceptions;

@SuppressWarnings("serial")
public class AnotherUserLoggedException extends Exception {
	public AnotherUserLoggedException() {
		super();
	}

	public AnotherUserLoggedException(String msg) {
		super(msg);
	}
}

package exceptions;



@SuppressWarnings("serial")
public class NoActivitysWithTagException extends Exception {
	public NoActivitysWithTagException() {
		super();
	}

	public NoActivitysWithTagException(String msg) {
		super(msg);
	}
}

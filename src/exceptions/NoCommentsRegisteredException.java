package exceptions;

@SuppressWarnings("serial")
public class NoCommentsRegisteredException extends Exception {
	public NoCommentsRegisteredException() {
		super();
	}

	public NoCommentsRegisteredException(String msg) {
		super(msg);
	}
}

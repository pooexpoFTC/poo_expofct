package exceptions;

@SuppressWarnings("serial")
public class EventAlreadyExistException extends Exception {
	public EventAlreadyExistException() {
		super();
	}

	public EventAlreadyExistException(String msg) {
		super(msg);
	}
}
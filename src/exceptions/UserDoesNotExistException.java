package exceptions;

@SuppressWarnings("serial")
public class UserDoesNotExistException extends Exception {
	public UserDoesNotExistException() {
		super();
	}

	public UserDoesNotExistException(String msg) {
		super(msg);
	}
}

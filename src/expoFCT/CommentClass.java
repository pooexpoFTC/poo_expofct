package expoFCT;

public class CommentClass implements Comment {
	private Event event;
	private Visitor author;
	private String comment;

	private int counterLikes;

	public CommentClass(Event event, Visitor author, String comment) {
		this.event = event;
		this.author = author;
		this.comment = comment;
		counterLikes = 0;
	}

	public Event getEvent() {
		return event;
	}

	public Visitor getAuthor() {
		return author;
	}

	public String getComment() {
		return comment;
	}

	public int addLike() {
		return ++counterLikes;
	}

	public int getLikes() {
		return counterLikes;
	}

	public String toString() {
		return this.getComment() + "; " + this.getAuthor().getEmail() + "; " + this.getLikes();
	}

	/**
	 * Comparacao por numero de likes, em caso de empate e feita por numero de comentarios feitos
	 * e em seguida por ordem alfabetica
	 */
	public int compareTo(Comment other) {
		if (this.getLikes() == other.getLikes()) {
			if (this.getAuthor().numberOfComments() != other.getAuthor().numberOfComments())
				return other.getAuthor().numberOfComments() - this.getAuthor().numberOfComments();
			else
				return this.getComment().compareTo(other.getComment());
		}
		return other.getLikes() - this.getLikes();
	}
}

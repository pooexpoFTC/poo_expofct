package expoFCT;

import java.util.HashSet;
import java.util.Set;

public class DepartmentClass implements Department {
	private String name;
	private String initials;
	private String building;
	private Set<Staff> members;

	public DepartmentClass(String name, String initials, String building) {
		this.name = name;
		this.initials = initials;
		this.building = building;
		members = new HashSet<Staff>();
	}

	public String getName() {
		return name;
	}

	public String getInitials() {
		return initials;
	}

	public String getBuilding() {
		return building;
	}

	public void addStaff(Staff member) {
		members.add(member);
	}

	public void removeStaff(Staff member) {
		members.remove(member);
	}

	public boolean containStaff(Staff member) {
		return members.contains(member);
	}

	public int numberOfMembers() {
		return members.size();
	}

	public String generatePassword() {
		return getInitials() + (numberOfMembers() + 1);
	}

	public boolean equals(Object other) {
		if (other != null && other instanceof DepartmentClass)
			return this.getName().equalsIgnoreCase(((Department) other).getName())
					|| this.getInitials().equalsIgnoreCase(((Department) other).getInitials());
		return false;
	}
}

package expoFCT;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class EventClass implements Event {

	private List<Comment> comments;
	private Map<String, Comment> commentByAuthorName;
	private Set<Visitor> subscribers;
	private String name;
	private String description;
	private Staff responsable;
	private int counterLikes, counterDislikes;

	public EventClass(String name, String description, Staff responsable) {
		this.name = name;
		this.description = description;
		comments = new LinkedList<Comment>();
		commentByAuthorName = new HashMap<String, Comment>();
		subscribers = new HashSet<Visitor>();
		counterDislikes = 0;
		counterLikes = 0;
		this.responsable = responsable;
	}

	public String getName() {
		return name;
	}

	public Staff getResponsable() {
		return responsable;
	}

	public String getDescription() {
		return description;
	}
	public Comment getCommentByAuthorName (String authorName){
		return commentByAuthorName.get(authorName);
	}
	public boolean hasSubscribe(Visitor subscribe) {
		return subscribers.contains(subscribe);
	}

	public void addComment(Comment comment) {
		comments.add(0, comment);
		commentByAuthorName.put(comment.getAuthor().getEmail(), comment);
	}

	public boolean addSubscriber(Visitor subscriber) {
		return subscribers.add(subscriber);
	}

	public int numberOfComments() {
		return comments.size();
	}

	public int numberOfSubscribers() {
		return subscribers.size();
	}

	public int addLike() {
		return ++counterLikes;
	}

	public int addDislike() {
		return ++counterDislikes;
	}

	public int getLikes() {
		return counterLikes;
	}

	public int getDislikes() {
		return counterDislikes;
	}

	public Iterator<Comment> iteratorOfComments() {
		return comments.iterator();
	}

	public Iterator<Visitor> iteratorOfVisitor() {
		return subscribers.iterator();
	}

	public String toString() {
		return getName() + "; "+ getResponsable().getEmail()+"; " + numberOfSubscribers() + "; " + numberOfComments() + "; " + getLikes() + "; "
				+ getDislikes();
	}

	/**
	 * Comparacao por numero de subscritores e desempate por numero de likes, em seguida por numero de dislikes,
	 * e por ordem alfabetica
	 */
	public int compareTo(Event other) {
		if (this.numberOfSubscribers() == other.numberOfSubscribers()) {
			if (this.getLikes() != other.getLikes())
				return other.getLikes() - this.getLikes();
			else if (this.getDislikes() != other.getDislikes())
				return this.getDislikes() - other.getDislikes();
			else
				return this.getName().compareTo(other.getName());
		}
		return  other.numberOfSubscribers() - this.numberOfSubscribers();
	}

}

package expoFCT;

import java.util.Comparator;

/**
 * Classe que implementa o metodo compare entre dois objectos
 */
public class ComparatorByComments implements Comparator<Event> {
	public int compare(Event e1, Event e2) {
		int comparation = e2.numberOfComments() - e1.numberOfComments();
		if (comparation == 0) {
			comparation = e2.getLikes() - e1.getLikes();
			if (comparation != 0)
				return comparation;
			else {
				comparation = e2.getDislikes() - e1.getDislikes();
				if (comparation != 0)
					return comparation;
				else
					return e1.getName().compareTo(e2.getName());
			}
		}
		return comparation;
	}
}
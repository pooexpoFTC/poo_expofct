package expoFCT;

public interface Staff extends User {
	/**
	 * Devolve o departamento ao qual o Staff pertence
	 * @return departamento ao qual o Staff pertence
	 */
	Department getDepartment();

	/**
	 * Adiciona o evento <code>event</code> a lista de eventos desse Staff
	 * @param event Evento a adicionar a lista
	 */
	void addEvent(Event event);

	/**
	 * Retorna o numero de eventos criados pelo Staff
	 * @return numero de eventos criados
	 */
	int numberOfEvents();

}

package expoFCT;

public interface User extends Comparable<User> {
	/**
	 * Devolve o email do utilizador
	 * @return email do utilizador
	 */
	String getEmail();
	/**
	 * Devolve a password do utilizador
	 * @return password do utilizador
	 */
	String getPassword();
}

package expoFCT;

import java.util.HashSet;
import java.util.Set;

public class StaffClass extends AbstractUserClass implements Staff{
	
	private Department department;
	private Set<Event> events;
	public StaffClass (String email, Department department){
		super(email,department.generatePassword());
		this.department = department;
		events = new HashSet<Event>();
		
	}
	
	public Department getDepartment(){
		return department;
	}
	public void addEvent(Event event){
		events.add(event);
	}
	public int numberOfEvents(){
		return events.size();
	}
	
	/**
	 * Comparacao pelo tipo de User, primeiro listar Admins e depois Staffs
	 */
	public int compareTo(User other){
		if(other instanceof AdminClass){
			return 1;
		}
		else if (other instanceof StaffClass)
			return super.getEmail().compareTo(other.getEmail());
			
		return -1;
	}
	
}

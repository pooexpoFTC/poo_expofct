package expoFCT;

public interface Department {
	/**
	 * Devolve o nome do departamento
	 * @return nome do departamento
	 */
	String getName();

	/**
	 * Devolve a sigla do departamento
	 * @return sigla do departamento
	 */
	String getInitials();

	/**
	 * Devolve o numero do departamento
	 * @return numero do departamento
	 */
	String getBuilding();

	/**
	 * Adiciona um User do tipo Staff ao departamento com o objeto Staff<code>member</code>
	 * @param member User do tipo Staff a adicionar
	 */
	void addStaff(Staff member);

	/**
	 * Remove um User do tipo Staff ao departamento com o objeto Staff<code>member</code>
	 * @param member User do tipo Staff a adicionar
	 */
	void removeStaff(Staff member);

	/**
	 * Devolve o numero de membros do departamento
	 * @return numero de membros
	 */
	int numberOfMembers();

	/**
	 * Devolve a password generada ataves da sua sigla e o numero de Membros do mesmo
	 * @return password generada ataves da sua sigla e o numero de Membros do mesmo
	 */
	String generatePassword();
}

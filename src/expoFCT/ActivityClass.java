package expoFCT;

import java.util.HashSet;
import java.util.Set;

public class ActivityClass extends EventClass implements Activity{
	private Set<String> tags;

	public ActivityClass(String name, String description, Staff responsable) {
		super(name, description, responsable);
		tags = new HashSet<String>();
	}

	public boolean addNewTag(String tagName){
		return tags.add(tagName);
	}
	public int numberOfTags(){
		return tags.size();
	}
	public boolean containTag(String tag){
		return tags.contains(tag);
	}
	public Department getDepartement(){
		return getResponsable().getDepartment();
	}
	
	public String toString(){
		return getName() + "; "+ getResponsable().getEmail() + "; " + getDepartement().getInitials() + "; " +
				numberOfSubscribers() +
				"; " + numberOfComments() + "; " + getLikes() +"; "
				+ getDislikes();
	}

}

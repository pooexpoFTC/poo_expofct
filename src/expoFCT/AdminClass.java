package expoFCT;

public class AdminClass extends AbstractUserClass implements Admin{
	public static int newPassord = 1;
	public AdminClass(String email) {
		super(email, "admin"+ newPassord++);
	}
	
	/**
	 * Comparacao por email para Users do tipo Admin
	 */
	public int compareTo(User other){
		if(other instanceof AdminClass){
			return super.getEmail().compareTo(other.getEmail());
		}
		return -1;
	}
}

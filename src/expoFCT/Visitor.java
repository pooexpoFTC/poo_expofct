package expoFCT;

public interface Visitor extends User {

	/**
	 * Adiciona o evento <code>event</code> a lista de eventos do Visitor
	 * @param event Evento a adicionar a lista
	 */
	void addEvent(Event event);

	/**
	 * Devolve o numero de eventos ao qual o Visitor ja participou
	 * @return numero de eventos ao qual o Visitor ja participou
	 */
	int numberOfEvents();

	/**
	 * Adiciona o comentario <code>comment</code>
	 * @param comment Comentario a adicionar a lista
	 */
	void addComment(Comment comment);

	/**
	 * Devolve o numero de comentarios feitos pelo Visitor
	 * @return numero de comentarios feitos pelo Visitor
	 */
	int numberOfComments();
}

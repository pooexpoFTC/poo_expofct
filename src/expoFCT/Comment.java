package expoFCT;

public interface Comment extends Comparable<Comment> {
	/**
	 * Devolve o evento ao pertence comentario
	 * @return evento ao pertence comentario
	 */
	Event getEvent();

	/**
	 * Devolve o autor do comentario
	 * @return autor do comentario
	 */
	Visitor getAuthor();

	/**
	 * Devolve o texto comentario
	 * @return comentario
	 */
	String getComment();

	/**
	 * Adiciona e devolve um like ao comentario
	 * @return numero de likes
	 */
	int addLike();

	/**
	 * Devolve o numero de likes do comentario
	 * @return numero de likes do comentario
	 */
	int getLikes();
}

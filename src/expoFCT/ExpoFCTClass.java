package expoFCT;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import exceptions.*;

public class ExpoFCTClass implements ExpoFCT {
	private Map<String, Event> events;
	private Map<String, SortedSet<Activity>> tags;
	private Map<String, User> users;
	private Map<String, Department> departments;
	private Comment topComment;
	private User logged;

	public ExpoFCTClass() {
		events = new HashMap<String, Event>();
		// inverseEvents = new LinkedList<Event>();
		tags = new HashMap<String, SortedSet<Activity>>();
		users = new HashMap<String, User>();
		departments = new HashMap<String, Department>();
		topComment = null;
		logged = null;
	}

	public User userLogged() {
		return logged;
	}

	public String registAdmin(String email) throws UserAlreadyExistException, UserLoggedException {
		if (logged != null)
			throw new UserLoggedException();
		Admin newUser = new AdminClass(email);
		if (users.put(email, newUser) != null)
			throw new UserAlreadyExistException();

		return newUser.getPassword();
	}

	public String registStaff(String email, String initials)
			throws UserAlreadyExistException, UserLoggedException, DepartmentDoesNotExistException {
		if (logged != null)
			throw new UserLoggedException();
		if (users.containsKey(email))
			throw new UserAlreadyExistException();
		Department dept = departments.get(initials);
		if (dept == null)
			throw new DepartmentDoesNotExistException();
		Staff newUser = new StaffClass(email, dept);
		dept.addStaff(newUser);

		users.put(email, newUser);

		return newUser.getPassword();
	}

	public String registVisitor(String email) throws UserAlreadyExistException, UserLoggedException {
		if (logged != null)
			throw new UserLoggedException();
		Visitor newUser = new VisitorClass(email);
		if (users.put(email, newUser) != null)
			throw new UserAlreadyExistException();
		return newUser.getPassword();
	}

	public void newEvent(String name, String description)
			throws UserCannotExecuteException, EventAlreadyExistException {
		if (logged == null || !(logged instanceof Staff))
			throw new UserCannotExecuteException();
		if (events.containsKey(name))
			throw new EventAlreadyExistException();
		Event newEvent = new EventClass(name, description, (Staff) logged);
		events.put(name, newEvent);
		((Staff) logged).addEvent(newEvent);
		// inverseEvents.add(0, newEvent);
	}

	public void newActivity(String name, String description, Set<String> setTags)
			throws UserCannotExecuteException, EventAlreadyExistException {
		if (logged == null || !(logged instanceof Staff))
			throw new UserCannotExecuteException();

		if (events.containsKey(name))
			throw new EventAlreadyExistException();
		Activity newActivity = new ActivityClass(name, description, (Staff) logged);
		events.put(name, newActivity);
		((Staff) logged).addEvent(newActivity);
		for (String tag : setTags) {
			if (tags.containsKey(tag))
				tags.get(tag).add(newActivity);
			else {
				SortedSet<Activity> newSet = new TreeSet<Activity>();
				newSet.add(newActivity);
				tags.put(tag, newSet);
			}
		}

	}

	public void addDepartment(String name, String initials, String building)
			throws UserCannotExecuteException, DepartmentAlreadyExistException {
		if (logged == null || !(logged instanceof Admin))
			throw new UserCannotExecuteException();
		Department department = new DepartmentClass(name, initials, building);
		if (departments.containsValue(department))
			throw new DepartmentAlreadyExistException();
		departments.put(initials, department);
	}

	public void logIn(String email, String password)
			throws UserDoesNotExistException, UserLoggedException, WrongPasswordException, AnotherUserLoggedException {
		if (users.get(email) == null)
			throw new UserDoesNotExistException();
		else if (logged != null) {
			if (logged.getEmail().equals(email))
				throw new UserLoggedException();
			else
				throw new AnotherUserLoggedException();
		}

		else if (!password.equals(users.get(email).getPassword()))
			throw new WrongPasswordException();
		logged = users.get(email);
	}

	public String logOut() throws UserNotLoggedException {
		if (logged == null)
			throw new UserNotLoggedException();
		String emailLogout = logged.getEmail();
		logged = null;
		return emailLogout;

	}

	public void participate(String nameEvent) throws UserCannotExecuteException, EventDoesNotExistException {
		Event event = events.get(nameEvent);
		if (logged == null || !(logged instanceof Visitor))
			throw new UserCannotExecuteException();
		if (event == null)
			throw new EventDoesNotExistException();
		event.addSubscriber((Visitor) logged);
		((Visitor) logged).addEvent(event);
	}

	public void comment(String nameEvent, String comment)
			throws UserCannotExecuteException, EventDoesNotExistException, NotRegisteredInEventException {
		if (logged == null || !(logged instanceof Visitor))
			throw new UserCannotExecuteException();
		Event event = events.get(nameEvent);
		Visitor author = (Visitor) logged;
		Comment newComment = new CommentClass(event, author, comment);
		if (event == null)
			throw new EventDoesNotExistException();
		if (!event.hasSubscribe(author))
			throw new NotRegisteredInEventException();
		event.addComment(newComment);
		author.addComment(newComment);
		if (topComment == null)
			topComment = newComment;

	}

	public int likeEvent(String nameEvent) throws EventDoesNotExistException {
		if (!events.containsKey(nameEvent)) {
			throw new EventDoesNotExistException();
		}
		return events.get(nameEvent).addLike();
	}

	public int dislikeEvent(String nameEvent) throws EventDoesNotExistException {
		if (!events.containsKey(nameEvent)) {
			throw new EventDoesNotExistException();
		}
		return events.get(nameEvent).addDislike();
	}

	public int likeComment(String nameEvent, String authorName)
			throws EventDoesNotExistException, UserDoesNotExistException, CommentDoesNotExistException {
		if (!events.containsKey(nameEvent)) {
			throw new EventDoesNotExistException();
		}
		if (!users.containsKey(authorName) || !(users.get(authorName) instanceof VisitorClass)) {
			throw new UserDoesNotExistException();
		}
		Comment comment = events.get(nameEvent).getCommentByAuthorName(authorName);
		if (comment == null) {
			throw new CommentDoesNotExistException();
		}
		comment.addLike();
		if (topComment.compareTo(comment) > 0)
			topComment = comment;
		return comment.getLikes();
	}

	public Comment getTopComment() throws NoCommentsRegisteredException {
		if (topComment == null)
			throw new NoCommentsRegisteredException();
		return topComment;
	}

	public Iterator<Activity> listByTag(String tag) throws NoActivitysWithTagException {
		if (!tags.containsKey(tag))
			throw new NoActivitysWithTagException();
		return tags.get(tag).iterator();
	}

	public Iterator<Comment> listCommentByEvent(String nameEvent) throws EventDoesNotExistException {
		if (!events.containsKey(nameEvent))
			throw new EventDoesNotExistException();
		return events.get(nameEvent).iteratorOfComments();
	}

	public Iterator<User> listUsers() throws UserCannotExecuteException {
		if (!(logged instanceof AdminClass))
			throw new UserCannotExecuteException();
		List<User> usersList = new ArrayList<User>(users.size());
		usersList.addAll(users.values());
		Collections.sort(usersList);
		return usersList.iterator();

	}

	public Iterator<Event> listEvents() {
		List<Event> eventList = new ArrayList<Event>(events.size());
		eventList.addAll(events.values());
		Collections.sort(eventList);
		return eventList.iterator();
	}

	public Iterator<Event> listByComments() {
		List<Event> eventList = new ArrayList<Event>(events.size());
		eventList.addAll(events.values());
		Collections.sort(eventList, new ComparatorByComments());
		return eventList.iterator();
	}
}

package expoFCT;

public interface Activity extends Event {
	
	/**
	 * Adiciona a tag com o nome <code>tagName</code> ao Set de tags
	 * @param tagName Nome da tag
	 * @return true se foi possivel adicionar, false caso contrario
	 */
	boolean addNewTag(String tagName);

	/**
	 * Retorna o numero de tags ja adicionadas
	 * @return numero de tags
	 */
	int numberOfTags();

	/**
	 * Verifica se a tag com o nome <code>tag</code> esta contida no Set de tags
	 * @param tag nome da tag
	 * @return true caso esteja contida, false caso contrario
	 */
	boolean containTag(String tag);

	/**
	 * Devolve um User do tipo Staff responsavel pela atividade
	 * @return User do tipo Staff responsavel pela atividade
	 */
	public Staff getResponsable();

	/**
	 * Devolve o departamento onde a atividade esta a ser realizada
	 * @return departamento onde a atividade esta a ser realizada
	 */
	Department getDepartement();
}

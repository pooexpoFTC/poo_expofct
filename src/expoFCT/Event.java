package expoFCT;

import java.util.Iterator;

public interface Event extends Comparable<Event> {
	/**
	 * Devolve o nome do evento
	 * @return nome do evento
	 */
	String getName();

	/**
	 * Devolve a descricao do evento
	 * @return descricao do evento
	 */
	String getDescription();

	/**
	 * Devolve o comentario que esta associado ao autor <code>authotName</code>
	 * @param authorName nome do autor do comentario
	 * @return comentario que esta associado ao autor <code>authotName</code>
	 */
	Comment getCommentByAuthorName(String authorName);

	/**
	 * Verifica se o User do tipo Visitor <code>subscriber</code> esta inscrito no evento
	 * @param subscribe User do tipo Visitor a confirmar a inscricao
	 * @return true se o <code>subscribe</code> esta inscrito, false caso contrario
	 */
	boolean hasSubscribe(Visitor subscribe);

	/**
	 * Adiciona um comentario <code>comment</code> ao evento 
	 * @param comment objeto do tipo Comment a ser adicionado
	 */
	void addComment(Comment comment);

	/**
	 * Adiciona o inscrito <code>subscriber</code> do tipo Visitor ao evento
	 * @param subscriber User do tipo Visitor a adicionar
	 * @return true caso seja possivel adicionar, false caso contrario
	 */
	boolean addSubscriber(Visitor subscriber);

	/**
	 * Adiciona um like ao evento
	 * @return numero de likes do evento
	 */
	int addLike();
	
	/**
	 * Adiciona um dislike ao evento
	 * @return numero de dislikes do evento
	 */
	int addDislike();

	/**
	 * Devolve o numero de likes do evento
	 * @return numero de likes do evento
	 */
	int getLikes();

	/**
	 * Devolve o numero de dislikes do evento
	 * @return numero de dislikes do evento
	 */
	int getDislikes();

	/**
	 * Devolve o numero de comentarios ja adicionados
	 * @return numero de comentarios ja adicionados
	 */
	int numberOfComments();

	/**
	 * Devolve o numero de inscritos ja adicionados
	 * @return numero de inscritos ja adicionados
	 */
	int numberOfSubscribers();

	/**
	 * Devolve um iterador para os comentarios do evento
	 * ordenados por ordem inversa de insercao
	 * @return iterador para os comentarios do evento
	 */
	Iterator<Comment> iteratorOfComments();

	/**
	 * Devolve um iterador para os User's do tipo Visitor do evento
	 * ordenados por ordem alfabetica
	 * @return iterador para os User's do tipo Visitor do evento
	 */
	Iterator<Visitor> iteratorOfVisitor();
}

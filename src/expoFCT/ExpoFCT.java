package expoFCT;

import java.util.Iterator;
import java.util.Set;

import exceptions.AnotherUserLoggedException;
import exceptions.CommentDoesNotExistException;
import exceptions.DepartmentAlreadyExistException;
import exceptions.DepartmentDoesNotExistException;
import exceptions.EventAlreadyExistException;
import exceptions.EventDoesNotExistException;
import exceptions.NoActivitysWithTagException;
import exceptions.NoCommentsRegisteredException;
import exceptions.NotRegisteredInEventException;
import exceptions.UserAlreadyExistException;
import exceptions.UserCannotExecuteException;
import exceptions.UserDoesNotExistException;
import exceptions.UserLoggedException;
import exceptions.UserNotLoggedException;
import exceptions.WrongPasswordException;

public interface ExpoFCT {

	public User userLogged();
	/**
	 * Regista um admin com o email <code>email</code>
	 * @param email identificador unico do User
	 * @return a password associada ao novo admin registado
	 * @throws UserAlreadyExistException se ja existir um user com o email dado
	 * @throws UserLoggedException  se existir um user ja com login feito
	 */
	public String registAdmin(String email) throws UserAlreadyExistException, UserLoggedException;

	/**
	 * Regista um staff com o email <code>email</code> e a sigla <code>initials</code> do departamento em que se vai
	 * registar
	 * 
	 * @param email identificar unico do User
	 * @param initials identificador do departamento onde se vai registar
	 * @return a password associada ao novo staff registado
	 * @throws UserAlreadyExistException se ja existir um user com o email dado
	 * @throws UserLoggedException se existir um user ja com login feito
	 * @throws DepartmentDoesNotExistException se o departamento nao existir
	 */
	String registStaff(String email, String initials)
			throws UserAlreadyExistException, UserLoggedException, DepartmentDoesNotExistException;

	/**
	 * Regista um visitor com o email<code>email</code> fornecido
	 * @param email identificador unico do User
	 * @return a password associada ao novo visitor registado
	 * @throws UserAlreadyExistException se ja existir um user com o email dado
	 * @throws UserLoggedException se existir um user ja com login feito
	 */
	String registVisitor(String email) throws UserAlreadyExistException, UserLoggedException;

	/**
	 * Regista um novo evento com o nome <code>name</code> e a descricao <code>description</code> dada em events e associa-o ao Staff que esta logged
	 * @param name nome do evento
	 * @param description descricao do evento
	 * @throws UserCannotExecuteException se o utilizador que esta logged nao for do tipo Staff
	 * @throws EventAlreadyExistException se o nome do evento ja estiver associado a outro evento
	 */
	void newEvent(String name, String description) throws UserCannotExecuteException, EventAlreadyExistException;

	/**
	 * Regista uma nova atividade com o nome <code>name</code> a descricao <code>description</code> e as tags <code>tags</code>
	 *  dada em events e associa-o ao Staff que esta logged
	 * @param name nome identificador da atividade
	 * @param description descricao da atividade
	 * @param setTags tags da atividade
	 * @throws UserCannotExecuteException se o utilizador que esta logged nao for do tipo Staff
	 * @throws EventAlreadyExistException se o nome do evento ja estiver associado a outro evento
	 */
	void newActivity(String name, String description, Set<String> setTags)
			throws UserCannotExecuteException, EventAlreadyExistException;

	/**
	 * Regista um novo departamento com o nome <code>name</code>, a sigla <code>initials</code> e o numero do edificio <code>building</code>
	 * @param name nome identificador do departamento
	 * @param initials sigla identificadora do departamento
	 * @param building numero do departamento
	 * @throws UserCannotExecuteException se o utilizador que esta logged nao for do tipo Admin
	 * @throws DepartmentAlreadyExistException se o nome do departamento ja estiver associado a outro departamento
	 */
	void addDepartment(String name, String initials, String building)
			throws UserCannotExecuteException, DepartmentAlreadyExistException;
			
	/**
	 * Faz o loggin ao User com o email <code>email</code> dado se a password <code>password</code> estiver correta
	 * @param email email do User
	 * @param password password para identificar a autenticidade
	 * @throws UserDoesNotExistException se o User com o email<code>email</code> dado nao existir
	 * @throws UserLoggedException se existir um user ja com login feito
	 * @throws WrongPasswordException se a password <code>password</code> estiver correta
	 * @throws AnotherUserLoggedException se houver outro user logged
	 */
	void logIn(String email, String password)
			throws UserDoesNotExistException, UserLoggedException, WrongPasswordException, AnotherUserLoggedException;

	/**
	 * Faz o logout ao User que estiver logged
	 * @return o User logged
	 * @throws UserNotLoggedException se o User nao tiver feito login
	 */
	String logOut() throws UserNotLoggedException;

	/**
	 * Inscreve o User logged ao evento com o nome <code>nameEvent</code>
	 * @param nameEvent nome do evento <code>nameEvent</code>
	 * @throws UserCannotExecuteException se o User nao for do tipo Visitor
	 * @throws EventDoesNotExistException se o evento com o nome <code>nameEvent</code> nao existir
	 */
	void participate(String nameEvent) throws UserCannotExecuteException, EventDoesNotExistException;

	/**
	 * Faz um comentario <code>comment</code> ao evento com o nome <code>nameEvent</code> na qual esta inscrito previamente
	 * @param nameEvent nome do evento <code>nameEvent</code>
	 * @param comment comentario dado <code>comment</code>
	 * @throws UserCannotExecuteException se o User nao for do tipo Visitor
	 * @throws EventDoesNotExistException  se o evento com o nome <code>nameEvent</code> nao existir
	 * @throws NotRegisteredInEventException se o Visitor esta a comentar um evento ao qual nao esta inscrito
	 */
	void comment(String nameEvent, String comment)
			throws UserCannotExecuteException, EventDoesNotExistException, NotRegisteredInEventException;

	/**
	 * Faz like ao evento com o nome <code>nameEvent</code>
	 * @param nameEvent nome do evento
	 * @return  retorna o numero de likes do evento 
	 * @throws EventDoesNotExistException se o evento com o nome <code>nameEvent</code> nao existir
	 */
	int likeEvent(String nameEvent) throws EventDoesNotExistException;

	/**
	 * Faz dislike ao evento com o nome <code>nameEvent</code>
	 * @param nameEvent nome do evento
	 * @return  retorna o numero de dislikes do evento 
	 * @throws EventDoesNotExistException se o evento com o nome <code>nameEvent</code> nao existir
	 */
	int dislikeEvent(String nameEvent) throws EventDoesNotExistException;

	/**
	 * Faz like ao comentario realizado pelo autor de nome <code>authorName</code> no evento com o nome <code>nameEvent</code> e 
	 * @param nameEvent nome do evento
	 * @param authorName nome do autor do comentario
	 * @return numero de likes do comentario onde fez like
	 * @throws EventDoesNotExistException se o evento com o nome <code>nameEvent</code> nao existir
	 * @throws UserDoesNotExistException se o User com o email<code>email</code> dado nao existir
	 * @throws CommentDoesNotExistException se o comentario a dar like nao existir
	 */
	int likeComment(String nameEvent, String authorName)
			throws EventDoesNotExistException, UserDoesNotExistException, CommentDoesNotExistException;

	/**
	 * Retorna o comentario com mais likes
	 * @return comentario com mais likes
	 * @throws NoCommentsRegisteredException se nao existir nenhum comentario
	 */
	Comment getTopComment() throws NoCommentsRegisteredException;

	/**
	 * Devolve um iterador para as atividades com a tag <code>tag</code> 
	 * @param tag tag a iterar
	 * @return iterador para todas as atividades com a tag <code>tag</code> 
	 * @throws NoActivitysWithTagException se nao houver atividades com a tag<code>tag</code>
	 */
	Iterator<Activity> listByTag(String tag) throws NoActivitysWithTagException;

	/**
	 * Devolve um iterador para os comentarios do evento pelo nome <code>nameEvent</code>
	 * @param nameEvent Nome do evento que contem os comentarios
	 * @return iterador para todos os comentarios do evento dado
	 * @throws EventDoesNotExistException se o evento com o nome <code>nameEvent</code> nao existir
	 */
	Iterator<Comment> listCommentByEvent(String nameEvent) throws EventDoesNotExistException;

	/**
	 * Devolve um iterador para todos os utilizadores
	 * primeiro apresenta os do tipo Admin, depois Staff e por ultimo Visitor
	 * usando a ordem alfabetica do email entre os utilizadores do mesmo tipo
	 * @return iterador para todos os utilizadores
	 * @throws UserCannotExecuteException se o User nao for do tipo Admin
	 */
	Iterator<User> listUsers() throws UserCannotExecuteException;
	
	/**
	 * Devolve um iterador para todos os eventos ordenados pela numero de inscritos
	 * em caso de empate e usado o numero de likes, seguido do numero de dislikes, e por fim a ordem alfabetica do nome do evento/atividade
	 * @return iterador para todos os eventos
	 */
	Iterator<Event> listEvents();

	/**
	 * Devolve um iterador para os eventos ordenados por comentarios
	 * em caso de empate e usado o numero de likes, seguido do numero de dislikes, e por fim a ordem alfabetica do nome do evento/atividade
	 * @return iterador para todos os eventos
	 */
	Iterator<Event> listByComments();
}

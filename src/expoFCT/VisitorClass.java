package expoFCT;

import java.util.HashSet;
import java.util.Set;

public class VisitorClass extends AbstractUserClass implements Visitor {
	public static int newPassord = 1;
	private Set<Event> events;
	private Set<Comment> comments;

	public VisitorClass(String email) {
		super(email, "visitor" + newPassord++);
		events = new HashSet<Event>();
		comments = new HashSet<Comment>();
	}

	public void addEvent(Event event) {
		events.add(event);
	}

	public int numberOfEvents() {
		return events.size();
	}

	public void addComment(Comment comment) {
		comments.add(comment);
	}

	public int numberOfComments() {
		return comments.size();
	}
	
	/**
	 * Comparacao por tipo de User, primeiro Admin, depois Staff e por fim Visitor
	 */
	public int compareTo(User other) {
		if (other instanceof AdminClass || other instanceof StaffClass) {
			return 1;
		}
		return super.getEmail().compareTo(other.getEmail());
	}


}

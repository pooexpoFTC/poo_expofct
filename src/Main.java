import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

import exceptions.*;
import expoFCT.*;

public class Main {

	private static final String EXITING = "Exiting.\n";

	private static final String PREMISSION_DENIED = "User cannot execute this command.\n";
	
	private static final String ADMIN = "ADMIN";
	private static final String STAFF = "STAFF";
	private static final String VISITOR = "VISITOR";
	
	private static final String USER_ADDED = "User was registered: ";
	private static final String USER_LOGGED_IN = "There is a user logged in.\n";
	private static final String ANOTHER_USER_LOGGED_IN = "Another user is logged in.\n";
	private static final String USER_ALREADY_EXIST = "User already exists.\n";
	private static final String USER_ALREADY_LOGGED_IN = "User already logged in.\n";
	private static final String NO_USER_LOGGED_IN = "No user is logged in.\n";
	private static final String USER_DOES_NOT_EXIST = "User does not exist.\n";
	
	private static final String EVENT_REGISTERED = "Event registered.\n";
	private static final String NOT_REGISTERED_IN_EVENT = "Not registered in the event.\n";
	private static final String EVENT_NAME_EXISTS = "Event name already exists.\n";
	private static final String REGISTERED_EVENT = "Registered in event.\n";
	private static final String EVENT_DOES_NOT_EXIST = "Event does not exist.\n";
	private static final String MOST_COMMENT_EVENTS_HEAD = "Most commented events:\n";

	private static final String ACTIVITY_REGISTERED = "Activity registered.\n";

	private static final String COMMENT_DOES_NOT_EXIST = "Comment does not exist.\n";
	private static final String NO_COMMENTS_REGISTERED = "No comments registered.\n";
	private static final String COMMENT_REGISTERED = "Comment registered.\n";

	private static final String DEPARTMENT_NOT_EXIST = "Department does not exist.\n";
	private static final String DEPARTMENT_REGISTERED = "Department registered.\n";
	private static final String DEPARTMENT_NAME_ID_ALREADY_EXISTS = "Department name or id already exists.\n";
	
	private static final String LIST_EVENTS_HEAD = "All events:";
	private static final String LIST_USERS_ADD = "All users:";
	
	private static final String WRONG_PASSWORD = "Wrong password.\n";

	private enum Command {
		EXIT, REGISTER, LOGIN, LOGOUT, NEWEVENT, NEWACTIVITY, DEPT, USERS, EVENTS, PARTICIPATE, COMMENT, LISTBYCOMMENTS, BYTAG, COMMENTS, LIKE, DISLIKE, LIKECOMMENT, TOPCOMMENT, UNKNOWN
	}

	public static void main(String[] args) {
		Main.interpreter();
	}

	private static Command getCommand(Scanner input) {
		try {
			return Command.valueOf(input.nextLine().toUpperCase());
		} catch (IllegalArgumentException e) {
			return Command.UNKNOWN;
		}
	}

	private static void interpreter() {
		ExpoFCTClass expoFCT = new ExpoFCTClass();
		Scanner input = new Scanner(System.in);
		Command c = getCommand(input);
		while (!c.equals(Command.EXIT)) {
			switch (c) {
			case REGISTER:
				register(input, expoFCT);
				break;
			case LOGIN:
				login(input, expoFCT);
				break;
			case LOGOUT:
				logout(expoFCT);
				break;
			case NEWEVENT:
				newEvent(input, expoFCT);
				break;
			case NEWACTIVITY:
				newActivity(input, expoFCT);
				break;
			case DEPT:
				dept(input, expoFCT);
				break;
			case USERS:
				listUsers(expoFCT);
				break;
			case EVENTS:
				listEvents(expoFCT);
				break;
			case PARTICIPATE:
				participate(input, expoFCT);
				break;
			case COMMENT:
				comment(input, expoFCT);
				break;
			case LISTBYCOMMENTS:
				listByComments(expoFCT);
				break;
			case COMMENTS:
				comments(input, expoFCT);
				break;
			case BYTAG:
				byTag(input, expoFCT);
				break;
			case LIKE:
				likeEvent(input, expoFCT);
				break;
			case DISLIKE:
				dislikeEvent(input, expoFCT);
				break;
			case LIKECOMMENT:
				likeComment(input, expoFCT);
				break;
			case TOPCOMMENT:
				topComment(expoFCT);
				break;
			default:
				break;
			}
			c = getCommand(input);
		}
		System.out.println(EXITING);
	}

	private static void register(Scanner in, ExpoFCTClass expoFCT) {
		String type = in.nextLine();
		String email = in.nextLine();
		String deptInitials = null;
		if (type.equalsIgnoreCase(STAFF))
			deptInitials = in.nextLine();
		try {
			switch (type) {
			case ADMIN:
				System.out.println(USER_ADDED + expoFCT.registAdmin(email) + ".\n");
				break;
			case STAFF:

				System.out.println(USER_ADDED + expoFCT.registStaff(email, deptInitials) + ".\n");
				break;
			case VISITOR:
				System.out.println(USER_ADDED + expoFCT.registVisitor(email) + ".\n");
				break;
			}

		} catch (UserLoggedException e) {
			System.out.println(USER_LOGGED_IN);
		} catch (UserAlreadyExistException e) {
			System.out.println(USER_ALREADY_EXIST);
		} catch (DepartmentDoesNotExistException e) {
			System.out.println(DEPARTMENT_NOT_EXIST);
		}
	}

	private static void login(Scanner in, ExpoFCTClass expoFCT) {
		String email = in.nextLine();
		String password = in.nextLine();
		try {
			expoFCT.logIn(email, password);
			System.out.println("Welcome " + email + ".\n");
		} catch (UserDoesNotExistException e) {
			System.out.println(USER_DOES_NOT_EXIST);
		} catch (UserLoggedException e) {
			System.out.println(USER_ALREADY_LOGGED_IN);
		} catch (AnotherUserLoggedException e) {
			System.out.println(ANOTHER_USER_LOGGED_IN);
		} catch (WrongPasswordException e) {
			System.out.println(WRONG_PASSWORD);
		}

	}

	private static void logout(ExpoFCTClass expoFCT) {
		try {
			System.out.println("Goodbye " + expoFCT.logOut() + ".\n");

		} catch (UserNotLoggedException e) {
			System.out.println(NO_USER_LOGGED_IN);
		}
	}

	private static void dept(Scanner in, ExpoFCTClass expoFCT) {
		String name = in.nextLine();
		String initials = in.nextLine();
		String building = in.nextLine();
		try {
			expoFCT.addDepartment(name, initials, building);
			System.out.println(DEPARTMENT_REGISTERED);
		} catch (UserCannotExecuteException e) {
			System.out.println(PREMISSION_DENIED);
		} catch (DepartmentAlreadyExistException e) {
			System.out.println(DEPARTMENT_NAME_ID_ALREADY_EXISTS);
		}
	}

	private static void listUsers(ExpoFCTClass expoFCT) {
		try {
			Iterator<User> it = expoFCT.listUsers();
			System.out.println(LIST_USERS_ADD);
			if (it.hasNext()) {
				while (it.hasNext()) {
					User user = it.next();
					if (user instanceof AdminClass)
						System.out.println(user.getEmail() + "; " + ADMIN);
					else if (user instanceof StaffClass)
						System.out.println(user.getEmail() + "; STAFF; " + ((Staff) user).numberOfEvents());
					else if (user instanceof VisitorClass)
						System.out.println(user.getEmail() + "; VISITOR; " + ((Visitor) user).numberOfEvents());
				}

			}
			System.out.println();
		} catch (UserCannotExecuteException e) {
			System.out.println(PREMISSION_DENIED);
		}
	}

	private static void listEvents(ExpoFCTClass expoFCT) {
		Iterator<Event> it = expoFCT.listEvents();
		System.out.println(LIST_EVENTS_HEAD);
		while (it.hasNext()) {
			Event ev = it.next();
			if (ev instanceof Activity) {
				Activity act = (Activity) ev;
				System.out.println(act);
			} else
				System.out.println(ev);
		}
		System.out.println();
	}

	private static void newEvent(Scanner in, ExpoFCTClass expoFCT) {
		String name = in.nextLine();
		String description = in.nextLine();
		try {
			expoFCT.newEvent(name, description);
			System.out.println(EVENT_REGISTERED);
		} catch (UserCannotExecuteException e) {
			System.out.println(PREMISSION_DENIED);
		} catch (EventAlreadyExistException e) {
			System.out.println(EVENT_NAME_EXISTS);
		}
	}

	private static void newActivity(Scanner in, ExpoFCTClass expoFCT) {
		String name = in.nextLine();
		String description = in.nextLine();
		int numberOfTags = in.nextInt();
		in.nextLine();
		Set<String> tagsList = new HashSet<String>();
		for (; numberOfTags > 0; numberOfTags--) {
			String tags = in.nextLine();
			tagsList.add(tags);
		}
		try {
			expoFCT.newActivity(name, description, tagsList);
			System.out.println(ACTIVITY_REGISTERED);
		} catch (UserCannotExecuteException e) {
			System.out.println(PREMISSION_DENIED);
		} catch (EventAlreadyExistException e) {
			System.out.println(EVENT_NAME_EXISTS);
		}

	}

	private static void participate(Scanner in, ExpoFCTClass expoFCT) {
		String nameEvent = in.nextLine();
		try {
			expoFCT.participate(nameEvent);
			System.out.println(REGISTERED_EVENT);
		} catch (UserCannotExecuteException e) {
			System.out.println(PREMISSION_DENIED);
		} catch (EventDoesNotExistException e) {
			System.out.println(EVENT_DOES_NOT_EXIST);
		}
	}

	private static void comment(Scanner in, ExpoFCTClass expoFCT) {
		String nameEvent = in.nextLine();
		String comment = in.nextLine();
		try {
			expoFCT.comment(nameEvent, comment);
			System.out.println(COMMENT_REGISTERED);
		} catch (UserCannotExecuteException e) {
			System.out.println(PREMISSION_DENIED);
		} catch (EventDoesNotExistException e) {
			System.out.println(EVENT_DOES_NOT_EXIST);
		} catch (NotRegisteredInEventException e) {
			System.out.println(NOT_REGISTERED_IN_EVENT);
		}
	}

	private static void listByComments(ExpoFCTClass expoFCT) {
		Iterator<Event> it = expoFCT.listByComments();
		System.out.println(MOST_COMMENT_EVENTS_HEAD);
		while (it.hasNext()) {
			Event ev = it.next();
			if (ev.numberOfComments() > 0)
				if (ev instanceof Activity) {
					Activity act = (Activity) ev;
					System.out.println(act);
				} else
					System.out.println(ev);
		}
		System.out.println();
	}

	private static void comments(Scanner in, ExpoFCTClass expoFCT) {
		try{
		String nameEvent = in.nextLine();
		Iterator<Comment> it = expoFCT.listCommentByEvent(nameEvent);
		System.out.println(nameEvent + " comments:");
		while (it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println();
		}catch(EventDoesNotExistException e){
			System.out.println(EVENT_DOES_NOT_EXIST);
		}
	}
		

	private static void byTag(Scanner in, ExpoFCTClass expoFCT) {
		String tag = in.nextLine();
		try{
			Iterator<Activity> it = expoFCT.listByTag(tag);
			System.out.println("Activities with tag "+ tag +":");
			while (it.hasNext()) {
				System.out.println(it.next());
			}
			System.out.println();
		}catch(NoActivitysWithTagException e){
			System.out.println("Activities with tag "+ tag +":\n");
		}
	}
	
	private static void likeEvent(Scanner in, ExpoFCTClass expoFCT) {
		String nameEvent = in.nextLine();
		try {
			
			System.out.println("Likes: " + expoFCT.likeEvent(nameEvent)+ "\n");
		} catch (EventDoesNotExistException e) {
			System.out.println(EVENT_DOES_NOT_EXIST);
		} 
	}
	private static void dislikeEvent(Scanner in, ExpoFCTClass expoFCT) {
		String nameEvent = in.nextLine();
		try {
			
			System.out.println("Dislikes: " + expoFCT.dislikeEvent(nameEvent)+ "\n");
		} catch (EventDoesNotExistException e) {
			System.out.println(EVENT_DOES_NOT_EXIST);
		} 
	}
	private static void likeComment(Scanner in, ExpoFCTClass expoFCT) {
		String nameEvent = in.nextLine();
		String authorName = in.nextLine();
		try {
			
			System.out.println("Comment likes: " + expoFCT.likeComment(nameEvent, authorName)+ "\n");
		} catch (EventDoesNotExistException e) {
			System.out.println(EVENT_DOES_NOT_EXIST);
		} catch(UserDoesNotExistException e){
			System.out.println(USER_DOES_NOT_EXIST);
		}catch(CommentDoesNotExistException e){
			System.out.println(COMMENT_DOES_NOT_EXIST);
		}
	}
	private static void topComment(ExpoFCTClass expoFCT) {
		try{
			System.out.println(expoFCT.getTopComment() + "\n");
		}catch(NoCommentsRegisteredException e){
			System.out.println(NO_COMMENTS_REGISTERED);
		}
	}
	
}